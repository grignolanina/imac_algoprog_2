#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// Array& newTab;
    // for(int i=0, i<toSort.size; i++){
    //     if(toSort[i]<toSort[i+1]){
    //         newTab[i]=toSort[i];
    //     } else {
    //         newTab[i]=toSort[i+1];
    //     }
    // }

    
    for(int i=0; i<toSort.size(); i++){

        int min = i;
        int stock = toSort[i];

        for(int j=i; j<toSort.size(); j++){

            if(toSort[j]<toSort[i]){
                min = j;
            }

        }
        toSort[i]=toSort[min];
        toSort[min]=stock;
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
