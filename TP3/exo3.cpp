#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->left=NULL;
        this->right=NULL;
        this->value=value;
        
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        //si valeur est sup à celle du noeud actuel => insere à droite / sinon insere à gauche
        if(value > this->value){
            if(this->right==NULL){
                this->right=(SearchTreeNode*)malloc(sizeof(Node));
                this->right = (SearchTreeNode*)createNode(value);
            }else{
                this->right->insertNumber(value);
            }
        } else {
            if(this->left==NULL){
                this->left=(SearchTreeNode*)malloc(sizeof(Node));
                this->left=(SearchTreeNode*)createNode(value);
            } else {
                this->left->insertNumber(value);
            }
        }

        //arbre equilibre
        //partir des noeuds de fin et calculer si dif avec premier >2 ou <-2

    //     //verif l'equilibre
    //     uint tailleGauche = this->left->height();
    //     uint tailleDroite = this->right->height();
    //     uint equilibre = tailleDroite-tailleGauche;


    //     if(this == NULL){
    //         return -1;
    //     } 

    //     if(equilibre == 2){
    //         uint equilibreDroit = (this->right->right->height()) - (this->left->left->height());
    //         if(equilibreDroit==-1){
    //             rotationDroite(this->right);
    //         }else{
    //             rotationGauche(this);
    //         }

    //     } else if (equilibre == -2){
    //         uint equilibreGauche = (this->right->right->height()) - (this->left->left->height());
    //         if(equilibreGauche == 1){
    //             rotationGauche(this->left);
    //         } else {
    //             rotationDroite(this);
    //         }

    //     }
        
    }

    // void rotationGauche(SearchTreeNode* arbre){
    //     if(arbre == NULL){
    //         return -1;
    //     }
    //     

    // }

    // void rotationDroite(){
    //    if(arbre == NULL){
    //         return -1;
    //     }

    // }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int hauteurDroite = 0;
        int hauteurGauche = 0;

        if(this->left==NULL && this->right==NULL){
            return 1;
        } else {
            if(this->left!=NULL){
                hauteurGauche += this->left->height();
            } 
            if(this->right!=NULL){
                hauteurDroite += this->right->height();
            }
            if(hauteurDroite>hauteurGauche){
                return hauteurDroite + 1;
            } else {
                return hauteurGauche + 1;
            }
        }
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int nbNoeudDroite = 0 ;
        int nbNoeudGauche = 0 ;


        if(this->left==NULL && this->right==NULL){
            return 1;
        } else {
            if(this->right!=NULL){
                nbNoeudDroite += this->right->nodesCount();
            }
            if(this->left!=NULL){
                nbNoeudGauche += this->left->nodesCount();
            }
            return nbNoeudDroite+nbNoeudGauche +1;
        }

	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->right==NULL && this->left==NULL){
            return true;
        } else {
            return false;
        }
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if(this->isLeaf()==true){
            leaves[leavesCount]=this;
            leavesCount++;
        } else {
            if(this->right!=NULL){
                this->right->allLeaves(leaves,leavesCount);
            }
            if(this->left!=NULL){
                this->left->allLeaves(leaves,leavesCount); 
            }
        }
        

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        //cherche le noeud de gauche
        if(this->left!=NULL){
            this->left->inorderTravel(nodes,nodesCount);
        }

        nodes[nodesCount]=this;
        nodesCount++;
        //revient à son niveau (this)

        //cherche le noeud de droite
        if(this->right!=NULL){
            this->right->inorderTravel(nodes,nodesCount);
        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount]=this;
        nodesCount++;

        if(this->left!=NULL){
            this->left->preorderTravel(nodes,nodesCount);
        }

        if(this->right!=NULL){
            this->right->preorderTravel(nodes,nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(this->left!=NULL){
            this->left->postorderTravel(nodes,nodesCount);
        }


        if(this->right!=NULL){
            this->right->postorderTravel(nodes,nodesCount);
        }


        nodes[nodesCount]=this;
        nodesCount++;

	}

	Node* find(int value) {
        // find the node containing value
        if(this->value != value ){
            
            if(this->right != NULL){
                this->right->find(value);
            } 
            if(this->left != NULL) {
                this->left->find(value);  
            }

        } else {
            return this;
        }


	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
